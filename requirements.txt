astunparse==1.6.3
blinker==1.6.2
chardet==5.1.0
click==8.1.3
colorama==0.4.6
Flask
itsdangerous==2.1.2
Jinja2==3.1.2
joblib==1.2.0
MarkupSafe==2.1.3
numpy
pandas
pyflowchart==0.2.3
python-dateutil==2.8.2
pytz==2023.3
scikit-learn==1.0.2
scipy
six==1.16.0
sklearn==0.0.post5
threadpoolctl==3.1.0
tzdata==2023.3
Werkzeug
gunicorn