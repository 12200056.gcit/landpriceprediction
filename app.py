import numpy as np
from flask import Flask, render_template, request
import pickle
import pandas as pd

app = Flask(__name__)
model = pickle.load(open("land_p.pkl",'rb'))

cols = ['Dzongkhag', 'Gewog', 'Chiwog', 'Category', 'Types']

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict():
    Dzongkhag = request.form['Dzongkhag']
    Gewog = request.form['Gewog']
    Chiwog = request.form['Chiwog']
    Category = request.form['Category']
    Types = request.form['Types']
    prediction = model.predict(
        pd.DataFrame(data={'Dzongkhag':Dzongkhag,'Gewog':Gewog,'Chiwog':Chiwog,'Category':Category,'Types':Types},
        index=[0])
    )
   
    output = round(prediction[0], 2)

    return render_template('index.html', prediction_text='Land Price is {}'.format(output))

# @app.route("/predict", methods=['POST'])
# def prediction():
#     if request.method == 'POST':
#         Dzongkhag = request.form['Dzongkhag']
#         Gewog = request.form['Gewog']
#         Chiwog = request.form['Chiwog']
#         Category = request.form['Category']
#         Types = request.form['Types']

#         sample = [[Dzongkhag, Gewog, Chiwog, Category, Types]]
#         x=pd.DataFrame(sample, columns=cols)
#         result = model.predict(x)
#         print(result)
#         return render_template("output.html", value=result)


if __name__ == "__main__":
    app.run(debug=True)